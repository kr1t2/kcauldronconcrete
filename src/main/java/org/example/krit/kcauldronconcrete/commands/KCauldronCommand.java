package org.example.krit.kcauldronconcrete.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.example.krit.kcauldronconcrete.KCauldronConcrete;
import org.jetbrains.annotations.NotNull;

public class KCauldronCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (args[0].equalsIgnoreCase("reload")) {
            if (sender instanceof Player p) {
                if (!p.hasPermission("kcauldronconcrete.reload")) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', KCauldronConcrete.getInstance().getConfig().getString("noreload-message")));
                    return true;
                }
            }
            KCauldronConcrete.getInstance().reloadConfig();
            if (sender instanceof Player p) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', KCauldronConcrete.getInstance().getConfig().getString("reload-message")));
            } else {
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', KCauldronConcrete.getInstance().getConfig().getString("reload-message")));
            }
        }


        return true;
    }
}
