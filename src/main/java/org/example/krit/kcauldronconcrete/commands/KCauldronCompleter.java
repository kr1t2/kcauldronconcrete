package org.example.krit.kcauldronconcrete.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class KCauldronCompleter implements TabCompleter {
    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, Command command, @NotNull String s, @NotNull String[] args) {

        if (args.length == 1) {
            return List.of("reload");
        }

        return null;
    }
}
