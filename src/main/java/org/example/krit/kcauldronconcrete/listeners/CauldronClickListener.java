package org.example.krit.kcauldronconcrete.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.example.krit.kcauldronconcrete.KCauldronUtils;

import static org.example.krit.kcauldronconcrete.KCauldronUtils.takeWaterFromCauldron;


public class CauldronClickListener implements Listener {
    @EventHandler
    public void onCauldronClick(PlayerInteractEvent e) {
        if (e.getClickedBlock() == null) {
            return;
        }
        if (e.getClickedBlock().getType() == Material.WATER_CAULDRON) {

            Player p = e.getPlayer();
            BlockData cauldronLevelCheck = e.getClickedBlock().getBlockData();

            if (p.getInventory().getItemInMainHand().getType().equals(Material.BLACK_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.BLACK_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            } else if (p.getInventory().getItemInMainHand().getType().equals(Material.GRAY_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.GRAY_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.LIGHT_GRAY_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.LIGHT_GRAY_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.WHITE_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.WHITE_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.BROWN_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.BROWN_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.YELLOW_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.YELLOW_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.ORANGE_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.ORANGE_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.RED_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.RED_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.PINK_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.PINK_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.PURPLE_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.PURPLE_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.MAGENTA_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.MAGENTA_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.LIGHT_BLUE_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.LIGHT_BLUE_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.BLUE_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.BLUE_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.CYAN_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.CYAN_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.GREEN_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.GREEN_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }else if (p.getInventory().getItemInMainHand().getType().equals(Material.LIME_CONCRETE_POWDER)) {
                e.setCancelled(true);
                doConcrete(p, Material.LIME_CONCRETE, e.getClickedBlock());
                takeWaterFromCauldron(e.getClickedBlock());
            }
        }
    }

    private void doConcrete(Player p, Material material, Block cauldron) {
        p.getInventory().getItemInMainHand().setAmount(p.getInventory().getItemInMainHand().getAmount() - 1);
        p.getInventory().addItem(new ItemStack(material));

        KCauldronUtils.spawnCloudParticles(p, cauldron.getLocation());
    }
}

