package org.example.krit.kcauldronconcrete.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.example.krit.kcauldronconcrete.KCauldronUtils;
import org.example.krit.kcauldronconcrete.Scheduler;

public class CauldronWithConcreteListener implements Listener {

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if (!e.getItemDrop().getItemStack().getType().toString().toUpperCase().contains("CONCRETE_POWDER")) {
            return;
        }
        Scheduler.runLater(e.getItemDrop().getLocation(), () -> {
            if (e.getItemDrop().getWorld().getBlockAt(e.getItemDrop().getLocation()).getType() != Material.WATER_CAULDRON) {
                return;
            }
            Block cauldron;
            try {
                cauldron = e.getItemDrop().getWorld().getBlockAt(e.getItemDrop().getLocation());
            } catch (NullPointerException exception) {
                return;
            }
            Item droppedItem = e.getItemDrop();

            if (droppedItem.getItemStack().getType().toString().length() <= 7) {
                return;
            }
            
            ItemStack newItem = new ItemStack(Material.valueOf(droppedItem.getItemStack().getType().toString().substring(0, droppedItem.getItemStack().getType().toString().length() - 7)));
            newItem.setAmount(droppedItem.getItemStack().getAmount());

            droppedItem.setItemStack(newItem);

            KCauldronUtils.takeWaterFromCauldron(cauldron);

            KCauldronUtils.spawnCloudParticles(e.getPlayer(), cauldron.getLocation());

        }, 20L);


    }
}
