package org.example.krit.kcauldronconcrete;

import org.bukkit.plugin.java.JavaPlugin;
import org.example.krit.kcauldronconcrete.commands.KCauldronCommand;
import org.example.krit.kcauldronconcrete.commands.KCauldronCompleter;
import org.example.krit.kcauldronconcrete.listeners.CauldronClickListener;
import org.example.krit.kcauldronconcrete.listeners.CauldronWithConcreteListener;

public final class KCauldronConcrete extends JavaPlugin {

    private static KCauldronConcrete plugin;

    @Override
    public void onEnable() {
        plugin = this;

        getConfig().options().copyDefaults();
        saveDefaultConfig();

        getServer().getPluginManager().registerEvents(new CauldronClickListener(), this);
        getServer().getPluginManager().registerEvents(new CauldronWithConcreteListener(), this);

        getCommand("kcauldronconcrete").setExecutor(new KCauldronCommand());
        getCommand("kcauldronconcrete").setTabCompleter(new KCauldronCompleter());
    }

    public static KCauldronConcrete getInstance() {
        return plugin;
    }
}
