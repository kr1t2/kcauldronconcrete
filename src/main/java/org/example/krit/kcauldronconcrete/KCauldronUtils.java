package org.example.krit.kcauldronconcrete;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class KCauldronUtils {

    public static void takeWaterFromCauldron(Block clickedBlock) {
        Levelled cauldronLevel = (Levelled) clickedBlock.getBlockData();
        int level = cauldronLevel.getLevel() - 1;
        if (level == 0) {
            clickedBlock.setType(Material.CAULDRON);
        } else {
            cauldronLevel.setLevel(level);
            clickedBlock.setBlockData(cauldronLevel);
        }
    }

    public static void spawnCloudParticles(Player p, Location location) {

        p.playSound(p.getLocation(), Sound.BLOCK_SPONGE_ABSORB, 1, 1);
        p.getWorld().spawnParticle(Particle.valueOf(KCauldronConcrete.getInstance().getConfig().getString("particles").toUpperCase()), location, KCauldronConcrete.getInstance().getConfig().getInt("particles-amount"));
    }
}
